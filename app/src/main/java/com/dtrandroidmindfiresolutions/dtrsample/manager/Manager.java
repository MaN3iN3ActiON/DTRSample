package com.dtrandroidmindfiresolutions.dtrsample.manager;

import com.dtrandroidmindfiresolutions.dtrsample.app.Constants;
import com.dtrandroidmindfiresolutions.dtrsample.models.Item;
import com.dtrandroidmindfiresolutions.dtrsample.restApi.ApiClient;
import com.dtrandroidmindfiresolutions.dtrsample.restApi.ApiInterface;
import com.dtrandroidmindfiresolutions.dtrsample.restApi.RestClient;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ychna on 19-10-16.
 */

public class Manager extends ApiClient{
    public ApiInterface apiInterface;

    public Manager(RestClient restClient){
        super(restClient);
        apiInterface = restClient.createAPIService(ApiInterface.class);
    }

    public Call<Item> fetchItem(int pathParam, Callback<Item> callback){
        Call<Item> call = apiInterface.getItem(pathParam,Constants.TOKEN);
        call.enqueue(callback);
        return call;
    }
}
