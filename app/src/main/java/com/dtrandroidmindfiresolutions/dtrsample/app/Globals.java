package com.dtrandroidmindfiresolutions.dtrsample.app;

import com.dtrandroidmindfiresolutions.dtrsample.restApi.RestClient;

/**
 * Created by ychna on 19-10-16.
 */

public class Globals {
    private static AppConfig sAppConfig;
    private static ServerConfig sServerConfig;
    private static RestClient restClient;

    public static void init() {
        sAppConfig = new AppConfig();
        sServerConfig = new ServerConfig();
    }
}
