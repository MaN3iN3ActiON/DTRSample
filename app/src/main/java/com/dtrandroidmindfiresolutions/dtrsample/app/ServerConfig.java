package com.dtrandroidmindfiresolutions.dtrsample.app;

/**
 * Created by ychna on 19-10-16.
 */

public class ServerConfig {
    public class API {
        public static final String ANDROID_VERSIONS = "android/jsonandroid";
    }

    public String getAPIRootURL() {
        return Constants.ROOT_URL;
    }
}
