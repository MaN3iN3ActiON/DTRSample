package com.dtrandroidmindfiresolutions.dtrsample.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dtrandroidmindfiresolutions.dtrsample.R;
import com.dtrandroidmindfiresolutions.dtrsample.manager.Manager;
import com.dtrandroidmindfiresolutions.dtrsample.models.Item;
import com.dtrandroidmindfiresolutions.dtrsample.restApi.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText id;
    TextView itemId,addedOn,updatedOn,addedBy,updatedBy,active,weight,sizeInfo,notes;
    Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proper);

        id =        (EditText)findViewById(R.id.id);
        itemId =    (TextView)findViewById(R.id.itemId);
        addedOn =   (TextView)findViewById(R.id.addedOn);
        updatedOn = (TextView)findViewById(R.id.updatedOn);
        addedBy =   (TextView)findViewById(R.id.addedBy);
        updatedBy = (TextView)findViewById(R.id.updatedBy);
        active =    (TextView)findViewById(R.id.active);
        weight =    (TextView)findViewById(R.id.weight);
        sizeInfo =  (TextView)findViewById(R.id.sizeInfo);
        notes =     (TextView)findViewById(R.id.notes);
        search =    (Button)findViewById(R.id.search);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestClient restClient = new RestClient();
                Manager manager = new Manager(restClient);
                Call<Item> call = manager.fetchItem(Integer.parseInt(id.getText().toString()), new Callback<Item>() {
                    @Override
                    public void onResponse(Call<Item> call, Response<Item> response) {
                        //itemDetails.setText(response.body().toString());
                        itemId.setText(response.body().getSku());
                        addedOn.setText(response.body().getAddedOn());
                        updatedOn.setText(response.body().getUpdatedOn());
                        addedBy.setText(String.valueOf(response.body().getAddedById()));
                        updatedBy.setText(String.valueOf(response.body().getUpdatedById()));
                        active.setText(String.valueOf(response.body().isActive()));
                        weight.setText(String.valueOf(response.body().getWeight()));
                        //sizeInfo.setText(response.body().getSizeInfo().toString());
                        notes.setText(response.body().getNotes());

                    }

                    @Override
                    public void onFailure(Call<Item> call, Throwable t) {

                    }
                });


            }
        });
    }
}
