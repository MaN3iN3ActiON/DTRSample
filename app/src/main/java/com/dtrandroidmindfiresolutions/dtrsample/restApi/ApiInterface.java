package com.dtrandroidmindfiresolutions.dtrsample.restApi;

import com.dtrandroidmindfiresolutions.dtrsample.models.Item;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by ychna on 19-10-16.
 */

public interface ApiInterface {// @Headers({"Authorization : Token 2278ac23e6e0121005026abb464446c56b7d1073"})
    @GET("items/{id}")
    Call<Item> getItem(@Path("id") int id , @Header("Authorization") String token);
}
