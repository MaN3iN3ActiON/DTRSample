package com.dtrandroidmindfiresolutions.dtrsample.restApi;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;

import retrofit2.Converter;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ychna on 19-10-16.
 */

public class GsonConverter {
    public static Converter.Factory getGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        gsonBuilder.serializeNulls();
        //DateTime.setFormats(Constants.DATE_FORMAT, Constants.DATE_TIME_FORMAT);
        //DateTime.setDateTimeTypeAdapter(gsonBuilder); //

        return GsonConverterFactory.create(gsonBuilder.create());
    }
}
