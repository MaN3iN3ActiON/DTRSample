package com.dtrandroidmindfiresolutions.dtrsample.restApi;

import com.dtrandroidmindfiresolutions.dtrsample.app.Constants;
import com.dtrandroidmindfiresolutions.dtrsample.app.Globals;

import retrofit2.Retrofit;

import static com.dtrandroidmindfiresolutions.dtrsample.restApi.GsonConverter.getGsonConverter;

/**
 * Created by ychna on 19-10-16.
 */

public class RestClient {
    private Retrofit restAdapter;

    public RestClient() {
        restAdapter = newRestAdapter();
    }

    public <T> T createAPIService(final Class<T> service) {
        return restAdapter.create(service);
    }

    private static Retrofit newRestAdapter() {
        return new Retrofit.Builder()
                .baseUrl(Constants.ROOT_URL)
                .addConverterFactory(getGsonConverter())
                .build();
    }
}
