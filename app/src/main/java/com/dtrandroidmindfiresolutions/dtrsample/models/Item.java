package com.dtrandroidmindfiresolutions.dtrsample.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ychna on 19-10-16.
 */

public class Item {
    private String id;
    private SizeInfo SizeInfo;
    private List<Image> Images = new ArrayList<Image>();
    @SerializedName("SKU")
    private String sku;
    private String notes;
    private boolean isActive;
    private Integer weight;
    private String addedOn;
    private String updatedOn;
    private Integer productId;
    private Integer sizeId;
    private Integer addedById;
    private Integer updatedById;
    private Integer vendorId;

    public Item(String notes,String id, SizeInfo SizeInfo, List<Image> Images, String sku, boolean isActive, Integer weight, String addedOn, String updatedOn, Integer productId, Integer sizeId, Integer addedById, Integer updatedById, Integer vendorId) {
        this.id = id;
        this.SizeInfo = SizeInfo;
        this.Images = Images;
        this.sku = sku;
        this.isActive = isActive;
        this.weight = weight;
        this.addedOn = addedOn;
        this.updatedOn = updatedOn;
        this.productId = productId;
        this.sizeId = sizeId;
        this.addedById = addedById;
        this.updatedById = updatedById;
        this.vendorId = vendorId;
        this.notes = notes;
    }

    @Override
    public String toString(){
        return "id :" + getId() + getSizeInfo() + getImages() + " SKU :" + getSku() +
                " isActive :" + isActive() + " weight :" + getWeight() + " addedOn :" + getAddedOn() +
                " updatedOn :" + getUpdatedOn() + " productId :" + getProductId() +
                " SizeId :" + getSizeId() + "addedByID :" + getAddedById() + "updatedById :" +
                getUpdatedById() + " vendorId :" + getVendorId() + "notes :" + getNotes();
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SizeInfo getSizeInfo() {
        return SizeInfo;
    }

    public void setSizeInfo(SizeInfo SizeInfo) {
        this.SizeInfo = SizeInfo;
    }

    public List<Image> getImages() {
        return Images;
    }

    public void setImages(List<Image> Images) {
        this.Images = Images;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(String addedOn) {
        this.addedOn = addedOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Integer getAddedById() {
        return addedById;
    }

    public void setAddedById(Integer addedById) {
        this.addedById = addedById;
    }

    public Integer getUpdatedById() {
        return updatedById;
    }

    public void setUpdatedById(Integer updatedById) {
        this.updatedById = updatedById;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }
}
