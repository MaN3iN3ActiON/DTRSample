package com.dtrandroidmindfiresolutions.dtrsample.models;

/**
 * Created by ychna on 19-10-16.
 */

public class Image {
    private String file;
    private String date;
    private Integer id;
    private String origFilename;

    public Image(String file, String date, Integer id, String origFilename) {
        this.file = file;
        this.date = date;
        this.id = id;
        this.origFilename = origFilename;
    }

    @Override
    public String toString(){
        return " Image" + " Date :" + getDate() + " File :" + getFile() +
                " OrigFilename :" + getOrigFilename() + " ImgId :" + getId() ;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getOrigFilename() {
        return origFilename;
    }

    public void setOrigFilename(String origFilename) {
        this.origFilename = origFilename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
