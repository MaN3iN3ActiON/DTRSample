package com.dtrandroidmindfiresolutions.dtrsample.models;

/**
 * Created by ychna on 19-10-16.
 */

public class SizeInfo {
    private Integer shoulder;
    private Integer bust;
    private Integer length;
    private Integer sleeve;
    private String notes;

    public SizeInfo(Integer shoulder, Integer bust, Integer length, Integer sleeve, String notes) {
        this.shoulder = shoulder;
        this.bust = bust;
        this.length = length;
        this.sleeve = sleeve;
        this.notes = notes;
    }

    @Override
    public String toString(){
        return "SizeInfo :" + " notes :" + getNotes() + " Bust :" +  getBust() +
                " Length :" + getLength() + " Shoulder :" +  getShoulder() +
                " Sleeve :" + getSleeve() ;
    }

    public Integer getShoulder() {
        return shoulder;
    }

    public void setShoulder(Integer shoulder) {
        this.shoulder = shoulder;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getSleeve() {
        return sleeve;
    }

    public void setSleeve(Integer sleeve) {
        this.sleeve = sleeve;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getBust() {
        return bust;
    }

    public void setBust(Integer bust) {
        this.bust = bust;
    }
}
